<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that pygments, a generic syntax highlighter, is vulnerable
to a CPU exhaustion attack via a crafted SML file.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.2.0+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your pygments packages.</p>

<p>For the detailed security status of pygments please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pygments">https://security-tracker.debian.org/tracker/pygments</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2590.data"
# $Id: $
