<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The wcsnrtombs function in all musl libc versions up through 1.2.1
has been found to have multiple bugs in handling of destination
buffer size when limiting the input character count, which can
lead to infinite loop with no forward progress (no overflow) or
writing past the end of the destination buffers.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.16-3+deb9u1.</p>

<p>We recommend that you upgrade your musl packages.</p>

<p>For the detailed security status of musl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/musl">https://security-tracker.debian.org/tracker/musl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2474.data"
# $Id: $
