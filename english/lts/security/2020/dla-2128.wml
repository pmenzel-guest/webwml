<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, incorrect implementation of Kerberos
GSSAPI and TGS requests or incorrect TLS handshakes.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7u251-2.6.21-1~deb8u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2128.data"
# $Id: $
