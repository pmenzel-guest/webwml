<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issue were found in Simple Linux Utility for Resource
Management (SLURM), a cluster resource management and job scheduling
system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6438">CVE-2019-6438</a>

    <p>SchedMD Slurm mishandles 32-bit systems, causing a heap overflow
    in xmalloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12838">CVE-2019-12838</a>

    <p>SchedMD Slurm did not escape strings when importing an archive
    file into the accounting_storage/mysql backend, resulting in SQL
    injection.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.03.9-5+deb8u5.</p>

<p>We recommend that you upgrade your slurm-llnl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2143.data"
# $Id: $
