<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs. In
particular it provides mitigations for the TAA (TSX Asynchronous Abort)
vulnerability. For affected CPUs, to fully mitigate the vulnerability it
is also necessary to update the Linux kernel packages as released in
DLA 1989-1.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20191115.2~deb8u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2051.data"
# $Id: $
