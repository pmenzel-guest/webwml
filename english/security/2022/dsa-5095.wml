<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36310">CVE-2020-36310</a>

    <p>A flaw was discovered in the KVM implementation for AMD processors,
    which could lead to an infinite loop.  A malicious VM guest could
    exploit this to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

    <p>Researchers at VUSec discovered that the Branch History Buffer in
    Intel processors can be exploited to create information side    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.</p>

    <p>This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

    <p>This is a similar issue to <a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>, but covers exploitation
    within a security context, such as from JIT-compiled code in a
    sandbox to hosting code in the same process.</p>

    <p>This is partly mitigated by disabling eBPF for unprivileged users
    with the sysctl: kernel.unprivileged_bpf_disabled=2.  This is
    already the default in Debian 11 <q>bullseye</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

    <p>A use-after-free was discovered in the MOXART SD/MMC Host Controller
    support driver. This flaw does not impact the Debian binary packages
    as CONFIG_MMC_MOXART is not set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

    <p>Yiqi Sun and Kevin Wang reported that the cgroup-v1 subsystem does
    not properly restrict access to the release-agent feature. A local
    user can take advantage of this flaw for privilege escalation and
    bypass of namespace isolation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

    <p>butt3rflyh4ck discovered a NULL pointer dereference in the UDF
    filesystem. A local user that can mount a specially crafted UDF
    image can use this flaw to crash the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25636">CVE-2022-25636</a>

    <p>Nick Gregory reported a heap out-of-bounds write flaw in the
    netfilter subsystem. A user with the CAP_NET_ADMIN capability could
    use this for denial of service or possibly for privilege escalation.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed
in version 5.10.103-1.  This update additionally includes many more
bug fixes from stable updates 5.10.93-5.10.103 inclusive.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5095.data"
# $Id: $
