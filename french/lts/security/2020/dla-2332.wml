#use wml::debian::translation-check translation="ca0b5f8b979d5badd7bdd56613a99ad2ac290a11" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Kevin Backhouse a découvert plusieurs vulnérabilités dans les dorsaux epson2
et epsonds de SANE, une bibliothèque pour les scanners. Un périphérique
malveillant distant pouvait exploiter cela pour déclencher une divulgation
d'informations, un déni de service et éventuellement une exécution de code
à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12862">CVE-2020-12862</a>

<p>Une lecture hors limites dans Backends de SANE avant la version 1.0.30 pouvait
permettre à un périphérique malveillant, connecté sur le même réseau local que
la victime, de lire des informations importantes, telles que l’adressage
ASLR du programme, alias GHSL-2020-082.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12863">CVE-2020-12863</a>

<p>Une lecture hors limites dans Backends de SANE avant la version 1.0.30 pouvait
permettre à un périphérique malveillant, connecté sur le même réseau local que
la victime, de lire des informations importantes, telles que l’adressage
ASLR du programme, alias GHSL-2020-083.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12865">CVE-2020-12865</a>

<p>Un dépassement de tampon de tas dans Backends de SANE avant la version 1.0.30
pouvait permettre à un périphérique malveillant, connecté sur le même réseau
local que la victime, d’exécuter du code arbitraire, alias GHSL-2020-084.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12867">CVE-2020-12867</a>

<p>Une déréférencement de pointeur NULL dans sanei_epson_net_read dans Backends
de SANE avant la version 1.0.30 permettait à un périphérique malveillant,
connecté sur le même réseau local que la victime, de provoquer un déni de
service, alias GHSL-2020-075.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.0.25-4.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sane-backends.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sane-backends, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sane-backends">https://security-tracker.debian.org/tracker/sane-backends</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2332.data"
# $Id: $
