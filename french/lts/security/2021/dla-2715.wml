#use wml::debian::translation-check translation="9196ab2fe6d75237e0e5ac47741ce145569a9cef" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a découvert qu’une allocation contrôlée par l’attaquant
utilisant la fonction alloca() pourrait aboutir à une corruption de mémoire,
permettant de planter systemd et par conséquent le système d’exploitation en
entier.</p>

<p>Plus de détails peuvent être trouvés dans l’annonce de Qualys sur
<a href="https://www.qualys.com/2021/07/20/cve-2021-33910/denial-of-service-systemd.txt">https://www.qualys.com/2021/07/20/cve-2021-33910/denial-of-service-systemd.txt</a></p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 232-25+deb9u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de systemd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/systemd">\
https://security-tracker.debian.org/tracker/systemd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2715.data"
# $Id: $
