#use wml::debian::translation-check translation="0f2e63875207f63029067f8878524f544b09a20f" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В веб-движке webkit2gtk были обнаружены следующие
уязвимости:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3862">CVE-2020-3862</a>

    <p>Срикант Гатта обнаружил, что вредоносный веб-сайт может вызывать отказ
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3864">CVE-2020-3864</a>

    <p>Райан Пикрен обнаружил, что контекст DOM-объектов может не иметь уникального
    источника безопасности.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3865">CVE-2020-3865</a>

    <p>Райан Пикрен обнаружил, что контекст DOM-объектов верхнего уровня может
    некорректно рассматриваться как безопасный.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3867">CVE-2020-3867</a>

    <p>Анонимный исследователь обнаружил, что обработка специально сформированного
    веб-содержимого может приводить к универсальному межсайтовому скриптингу.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3868">CVE-2020-3868</a>

    <p>Марсин Товальский обнаружил, что обработка специально сформированого веб-содержимого
    может приводить к выполнению произвольного кода.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2.26.4-1~deb10u1.</p>

<p>Рекомендуется обновить пакеты webkit2gtk.</p>

<p>С подробным статусом поддержки безопасности webkit2gtk можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4627.data"
