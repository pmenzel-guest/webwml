msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: L10N Debian Indonesian <debian-l10n-indonesian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Language: id\n"
"X-Generator: Poedit 3.0\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Sistem Operasi Universal"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "Foto Grup DC19"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "Foto Grub DebConf19"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "Mini DebConf Hamburg 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "Foto grup MiniDebConf di Hamburg 2018"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Cuplikan layar Installer Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Cuplikan layar dari installer Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian seperti Swiss Army Knife"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "Orang-orang bersenang-senang dengan Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Orang-orang Debian saat Debconf18 di Hsinchu benar-benar bersenang-senang"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Sedikit dari Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronews"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronews dari Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planet"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Planet Debian"
