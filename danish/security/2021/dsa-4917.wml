#use wml::debian::translation-check translation="1fe97ede528cadc78447b1edeb9a753b00b0639c" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i webbrowseren chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30506">CVE-2021-30506</a>

    <p>@retsew0x01 opdagede en fejl i Web Apps' 
    installeringsgrænseflade.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30507">CVE-2021-30507</a>

    <p>Alison Huffman opdagede en fejl i Offline-tilstand.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30508">CVE-2021-30508</a>

    <p>Leecraso og Guang Gong opdagede et bufferoverløbsproblem i 
    implementeringen af Media Feeds.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30509">CVE-2021-30509</a>

    <p>David Erceg opdagede et problem med skrivning udenfor grænserne i 
    implementeringen af Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30510">CVE-2021-30510</a>

    <p>Weipeng Jiang opdagede en kapløbstilstand i windowmanageren 
    aura.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30511">CVE-2021-30511</a>

    <p>David Erceg opdagede et problem med læsning udenfor grænserne i 
    implementeringen af Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30512">CVE-2021-30512</a>

    <p>ZhanJia Song opdagede et problem med anvendelse efter frigivelse i 
    notifikationsimplementeringen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30513">CVE-2021-30513</a>

    <p>Man Yue Mo opdagede en ukorrekt type i JavaScript-biblioteket 
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30514">CVE-2021-30514</a>

    <p>koocola og Wang opdagede et problem med anvendelse efter frigivelse i 
    Autofill-funktionaliteten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30515">CVE-2021-30515</a>

    <p>Rong Jian og Guang Gong opdagede et problem med anvendelse efter 
    frigivelse i API'et til filsystemadgang.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30516">CVE-2021-30516</a>

    <p>ZhanJia Song opdagede et bufferoverløbsproblem i 
    browserhistorikken.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30517">CVE-2021-30517</a>

    <p>Jun Kokatsu opdagede et bufferoverløbsproblem i 
    læsningstilstand.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30518">CVE-2021-30518</a>

    <p>laural opdagede anvendelse af en ukorrekt type i JavaScript-biblioteket 
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30519">CVE-2021-30519</a>

    <p>asnine opdagede et problem med anvendelse efter frigivelse i 
    Payments-funktionaliteten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30520">CVE-2021-30520</a>

    <p>Khalil Zhani opdagede et problem med anvendelse efter frigivelse i 
    implementeringen af Tab Strip.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 90.0.4430.212-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine chromium-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende chromium, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4917.data"
